#!/bin/bash

d=$(date '+%y%m%d')
sha=$(git rev-parse --short=8  HEAD)

tag="$d-${sha}"

v1="-Dspring.profiles.active=h2"   ###use it when you want to run the docker file, change another v1 as comment 
#v1=""                             ##use it for docker compose 

v2="release-0.0.1-SNAPSHOT-${tag}.jar"  
 
                                                    
docker build -t springapp:$tag  --build-arg value1=${v1}  --build-arg value2=${v2} .


